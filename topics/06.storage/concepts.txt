Volumes:
    In Kubernetes, volumes are a way to provide persistent storage to containers running within Pods. Volumes enable data to survive container restarts, rescheduling, and even Pod termination.
    They are crucial for various use cases, such as storing application data, configuration files, or shared data between containers within the same Pod.
    
    Kubernetes supports various volume types to accommodate different storage needs:
        emptyDir: A temporary volume that is created when a Pod is assigned to a node and deleted when the Pod is removed.
        hostPath: Mounts a file or directory from the host node's filesystem into the Pod.
        nfs: Uses a Network File System (NFS) share as a volume.
        persistentVolumeClaim: Provides access to persistent storage through a Persistent Volume Claim (PVC), which abstracts the underlying storage infrastructure.
        Cloud-specific volumes like awsElasticBlockStore, azureDisk, and gcePersistentDisk for cloud providers.
    
    Kubernetes can dynamically provision storage volumes based on PVC requests when a StorageClass is defined. This simplifies storage management and allocation.
    Volumes can be mounted in read-only mode, allowing a container to access data without modifying it.
    Kubernetes supports volume modes like FileOrCreate and DirectoryOrCreate to create files or directories within volumes.

    Static provisioning involves manually creating PVs and binding them to PVCs, providing fine-grained control over the storage configuration.
    Dynamic provisioning, on the other hand, automates the creation of PVs based on user-defined StorageClasses, simplifying the process and making it more responsive to changing storage needs

Storage classes:
    StorageClass is an abstraction layer that defines how persistent storage should be provisioned and managed. StorageClasses are used to dynamically provision Persistent Volumes (PVs) and are essential for automating the management of storage resources in a cluster.


StatefulSet:
    A StatefulSet is a Kubernetes resource that is used to deploy and manage stateful applications. Unlike Deployments, which are suitable for stateless services, StatefulSets are designed for applications that require stable, unique network identities, stable storage,
    and ordered scaling. Each Pod managed by a StatefulSet has a unique, ordinal index and a stable hostname.

    Update strategies:
        A StatefulSet's .spec.updateStrategy field allows you to configure and disable automated rolling updates for containers, labels, resource request/limits, and annotations for the Pods in a StatefulSet.
        There are two possible values:
            OnDelete:
                When a StatefulSet's .spec.updateStrategy.type is set to OnDelete, the StatefulSet controller will not automatically update the Pods in a StatefulSet. Users must manually delete Pods to cause the controller to create new Pods that reflect modifications made to a StatefulSet's .spec.template.
            RollingUpdate:
                The RollingUpdate update strategy implements automated, rolling updates for the Pods in a StatefulSet. This is the default update strategy.

        The Pods in the StatefulSet are updated in reverse ordinal order.  Note that, even though the StatefulSet controller will not proceed to update the next Pod until its ordinal successor is Running and Ready,
        it will restore any Pod that fails during the update to its current version.

        Patch the web StatefulSet to apply the RollingUpdate update strategy:
        > kubectl patch statefulset web -p '{"spec":{"updateStrategy":{"type":"RollingUpdate"}}}'
        
        In one terminal window, patch the web StatefulSet to change the container image again:
        > kubectl patch statefulset web --type='json' -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/image", "value":"gcr.io/google_containers/nginx-slim:0.8"}]'

    Headless Service:
        A Headless Service in Kubernetes is a type of service that doesn't allocate a cluster-internal IP address and does not load balance traffic to its pods. Instead, it's used to discover and access individual pods directly using their DNS names. Headless Services are typically used in scenarios where you need to 
        interact with each pod of a set of pods individually, such as in StatefulSets or when performing database operations with replicas.

        Here are key characteristics and use cases for Headless Services:

        No Cluster IP: Headless Services do not allocate a Cluster IP address. As a result, they don't provide load balancing or routing of traffic to the pods.
        DNS-Based Service Discovery: The primary purpose of a Headless Service is to provide DNS-based service discovery. Each pod in the service has a DNS entry in the format <pod-name>.<service-name>.<namespace>.svc.cluster.local, and this DNS entry resolves to the individual pod's IP address.
        Use Cases:
            StatefulSets: Headless Services are commonly used with StatefulSets to enable stable network identities for each pod in the StatefulSet. This is crucial for databases, message queues, and other stateful applications where each instance has its own unique identity.
            Database Replication: When working with replicated databases like MySQL or PostgreSQL, Headless Services can be used to target a specific database replica for read or write operations.
            Service Discovery Example:
            Let's say you have a StatefulSet managing three replicas of a database, and you've created a Headless Service for it. You can connect to a specific database replica using its DNS name:

                # Connect to the first database replica
                mysql -h replica-0.my-database-service.my-namespace.svc.cluster.local -u <username> -p

                # Connect to the second database replica
                mysql -h replica-1.my-database-service.my-namespace.svc.cluster.local -u <username> -p

                # Connect to the third database replica
                mysql -h replica-2.my-database-service.my-namespace.svc.cluster.local -u <user>
        
        when you create a StatefulSet in Kubernetes, it's a common practice to create a corresponding Headless Service for it. The Headless Service provides DNS-based service discovery and stable network identities for the pods managed by the StatefulSet, which is often essential for stateful applications.

        In summary, a Headless Service in Kubernetes is a special type of service that provides DNS-based service discovery without allocating a Cluster IP or performing load balancing. It's particularly useful for stateful applications where direct access to individual pods is required, such as in StatefulSets or database replication scenarios.
        